#ifndef HAMMIN_H
#define HAMMIN_H

#include <iostream>
#include <fstream>
class Hamming74{
private:

public:
  static char* G;
  static char* H;
  static char* R;
  static void init(){
    //the bits are reversed for good operation of and
    Hamming74::G = new char[7];
    Hamming74::G[0] = 0xB0;   //1101 0000
    Hamming74::G[1] = 0xD0;   //1011 0000
    Hamming74::G[2] = 0x10;   //1000 0000
    Hamming74::G[3] = 0xE0;   //0111 0000
    Hamming74::G[4] = 0x20;   //0100 0000
    Hamming74::G[5] = 0x40;   //0010 0000
    Hamming74::G[6] = 0x80;   //0001 0000

    Hamming74::H = new char[3];
    Hamming74::H[0] = 0xAA;   //1010101 0
    Hamming74::H[1] = 0xCC;   //0110011 0
    Hamming74::H[2] = 0xF0;   //0001111 0

    Hamming74::R = new char[4];
    Hamming74::R[0] = 0x08;   //0010000 0
    Hamming74::R[1] = 0x20;   //0000100 0
    Hamming74::R[2] = 0x40;   //0000010 0
    Hamming74::R[3] = 0x80;   //0000001 0
  }

  static char highNibble2Hamming7(char high){
    high &= 0xF0; // assert low low nibble
    char mul;
    char out = 0x00;
    for(short i = 0; i<7; i++){
      mul = Hammin74::G[i] & high;
      if(getParity(mul) == 1)
        out |= (0x01 << i);
      else
        out &= ~(0x01 << i);
      //check parity;
    }
    return (out << 1); // ignore bit 0
  }

  static void hammingEncodeByte(char byte, char &out1, char &out2){
    out1 = highNibble2Hamming7(byte);
    out2 = highNibble2Hamming7(byte << 4);
  }

  static unsigned short hammingCheckError(char byte){
    byte &= 0xFE;
    unsigned short z = 0;
    char mul;
    for(short i = 0; i < 3; i++){
      mul = Hammin74::H[i] & byte;
      if(getParity(mul) == 1)
        z |= (0x01 << i);
      else
        z &= ~(0x01 << i);
    }
    //std::cout << z << std::endl;
    return z;
  }

  static char hammingCorrectByte(char byte, short bit){ // only seven bits
    char toggle = byte ^ (0x01 << bit);
    return toggle;
  }

  static char hammingDecodeByte(char h_byte){
    unsigned short z = hammingCheckError(h_byte);
    char decode = 0x00;
    if(z != 0){ // error
      h_byte = hammingCorrectByte(h_byte, z);
    }
    char mul;
    for(int i = 0; i < 4; i++){
      mul = Hammin74::R[i] & h_byte;
      if(getParity(mul) == 1)
        decode |= (0x01 << i);
      else
        decode &= ~(0x01 << i);
    }
    return decode;
  }

  static char hammingReconstrucData(char c1, char c2){
    char data = hammingDecodeByte(c1);
    data = ((data << 4) | (hammingDecodeByte(c2) & 0x0F));
    return data;
  }

  static int getParity(char c){
    int parity = 0;
    while(c){
      parity = !parity;
      c = c & (c - 0x01);
    }
    return parity;
  }

  static void hammingEncodeFile(std::string ifilename, std::string ofilename){
    std::ifstream ifs;
    std::ofstream ofs;
    ifs.open(ifilename, std::ios::binary);
    ofs.open(ofilename + ".hex", std::ios::binary);
    if(!ifs.is_open() || !ofs.is_open()) return;
    char c;
    char enc1, enc2;
    while(ifs.get(c)){
      Hamming74::hammingEncodeByte(c, enc1, enc2);
      ofs << enc1 << enc2;
    }
    ifs.close();
    ofs.close();
  }

  static void hammingDecodeFile(std::string ifilename, std::string ofilename){
    std::ifstream ifs;
    std::ofstream ofs;
    ifs.open(ifilename, std::ios::binary);
    ofs.open(ofilename, std::ios::binary);
    if(!ifs.is_open() || !ofs.is_open()) return;
    char c1, c2;
    char dec;
    while(ifs.get(c1)){
      if(ifs.get(c2)){
        dec = Hamming74::hammingReconstrucData(c1, c2);
        ofs << dec;
      }
    }
    ifs.close();
    ofs.close();
  }

};

char* Hamming74::G = nullptr;
char* Hamming74::H = nullptr;
char* Hamming74::R = nullptr;

#endif
