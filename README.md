Implementation of hammin(7,4) encoding/decoding


Compile with g++ in std c++11 the file hammin_main.cpp

Compile Options:
  Define DECODE (-D DECODE) for compile the decoder
  otherwise always compile the encoder

  Define PIPE_STDOUT (-D PIPE_STDOUT) for output the file int std::out
  (for using pipe >)
