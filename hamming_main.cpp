#include "hamming.h"
#include <iostream>
#include <fstream>
#include <string>


#ifdef DECODE

int main(int argc, char const *argv[]) {

  Hamming74::init();
#ifdef PIPE_STDOUT
  if (argc != 2) {
      std::cerr << "\nUsage: arg1->file to decode\n";
      return 0;
  }
  Hamming74::hamminDecodeFile(std::string(argv[1]), "hamming_temp.txt");
  std::ifstream ifs;
  ifs.open("hamming_temp.txt");
  if(!ifs.is_open()) return;
  char c;
  while (ifs.get(c)) {
    std::cout << c;
  }
#else
  if (argc != 3) {
    std::cerr << "\nUsage: arg1->file to decode; arg2->output name(include extension) \n";
    return 0;
  }
  Hamming74::hamminDecodeFile(std::string(argv[1]), std::string(argv[2]));
#endif

  return 0;
}

#else

int main(int argc, char const *argv[]) {

  Hammin74::init();

#ifdef PIPE_STDOUT
  if (argc != 2) {
      std::cerr << "\nUsage: arg1->file to encode \n";
      return 0;
  }
  Hammin74::hamminEncodeFile(std::string(argv[1]), "hamming_temp");
  std::ifstream ifs;
  ifs.open("hamming_temp.hex", std::ios::binary);
  if(!ifs.is_open()) return;
  char c;
  while (ifs.get(c)) {
    std::cout << c;
  }
#else
  if (argc != 3) {
      std::cerr << "\nUsage: arg1->file to encode; arg2->output name(without extension) \n";
      return 0;
  }
  Hammin74::hamminEncodeFile(std::string(argv[1]), std::string(argv[2]));
#endif
  return 0;
}

#endif
